from app import app
from flask import Flask, redirect, url_for, request, Response
import json
import datetime
import sqlite3

@app.route('/add',methods = ['POST'])
def add():
	requestApiKey = request.headers.get("APIKEY")
	if not requestApiKey == "CRSGS>Jo)u@UBwYJ":
		print("Access denied!")
		return Response(status=403)
	jsonData = request.get_json()
	uuid = jsonData.get("uuid")
	puuid = jsonData.get("puuid")
	deviceId = jsonData.get("deviceId")

	connection = sqlite3.connect("unique.db")
	cursor = connection.cursor()
	now = datetime.datetime.now()
	insertCMD = "INSERT INTO uuids (uuid, puuid, deviceId, timedate) VALUES (?, ?, ?, ?);"
	cursor.execute(insertCMD, (uuid, puuid, deviceId, now))
	connection.commit()
	
	print("New UUID added: %s %s" % (uuid, puuid))
	
	selectCMD = "SELECT COUNT(id) FROM uuids WHERE uuid == ? AND deviceId != ?"
	cursor.execute(selectCMD, (uuid, deviceId))
	result=cursor.fetchone()
	number_of_rows=result[0]

	
	connection.close()
	data = {"unique": number_of_rows == 0}

	return Response(response=json.dumps(data), status=201, mimetype="application/json")
