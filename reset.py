import sqlite3
connection = sqlite3.connect("unique.db")

cursor = connection.cursor()

cursor.execute("DROP TABLE uuids;")
cursor.execute("CREATE TABLE uuids(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,uuid VARCHAR (256) NOT NULL,puuid VARCHAR (256) NOT NULL,deviceId VARCHAR (256) NOT NULL,timedate DATETIME NOT NULL);")

connection.commit()
connection.close()